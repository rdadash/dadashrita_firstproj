function toggleSidebar() {
  if (window.matchMedia("(min-width: 992px)").matches) {
    var element = document.getElementById("sidebar");
    element.classList.toggle("sidebar-mini");
  }
}